from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from Entities.frame import Frame
from Entities.metadata import Metadata
from Entities.singelton import Singleton
from Entities.video import Video


class RationalQuery(metaclass=Singleton):
    def __init__(self, url: str):
        self.engine = create_engine(url, echo=True)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def add_frame(self, frame: Frame):
        self.session.add(frame)
        self.session.commit()

    def add_metadata(self, metadata: Metadata):
        self.session.add(metadata)
        self.session.commit()

    def add_video(self, video: Video):
        self.session.add(video)
        self.session.commit()

    def get_video_path(self, video_id):
        video = self.get_video(video_id)
        return video.os_path

    def get_video(self, video_id: int):
        video = self.session.query(Video).get(video_id)
        return video

    def get_all_videos(self):
        return self.session.query(Video).all()

    def get_all_path_videos(self):
        videos = self.get_all_videos()
        return [video.os_path for video in videos]

    def get_frame(self, frame_id: int):
        frame = self.session.query(Frame).get(frame_id)
        return frame

    def get_metadata(self, metadata_id: int):
        metadata = self.session.query(Metadata).get(metadata_id)
        return metadata

    def get_all_frames(self):
        return self.session.query(Frame).all()

    def get_all_metadata(self):
        return self.session.query(Metadata).all()

    def __del__(self):
        print("Destructor called")
