from sqlalchemy import Column, Float, Integer
from sqlalchemy.orm import relationship
from Entities.base import Base


class Metadata(Base):
    __tablename__ = 'metadata'
    metadata_id = Column(Integer, primary_key=True)
    fov = Column(Float)
    azimuth = Column(Float)
    elevation = Column(Float)
    frames = relationship('Frame', backref="Metadata", cascade="save-update")

    def __init__(self, metadata_id: int, fov: float, azimuth: float, elevation: float):
        self.metadata_id = metadata_id
        self.fov = fov
        self.azimuth = azimuth
        self.elevation = elevation

    def __repr__(self):
        return f'{self.frames} - {self.metadata_id} - {self.fov} - {self.elevation} - {self.azimuth}'
