from sqlalchemy import Column, String, Integer, ForeignKey
from Entities.base import Base


class Frame(Base):
    __tablename__ = 'frame'
    frame_id = Column(Integer, primary_key=True)
    os_path = Column(String)
    frame_index = Column(Integer)
    metadata_id = Column(Integer, ForeignKey('metadata.metadata_id'))
    video_id = Column(Integer, ForeignKey('video.video_id'))

    def __init__(self, frame_id: int, os_path: str, frame_index: int, metadata_id: int, video_id: int):
        self.frame_id = frame_id
        self.os_path = os_path
        self.frame_index = frame_index
        self.metadata_id = metadata_id
        self.video_id = metadata_id

    def __repr__(self):
        return f'{self.metadata_id} - {self.frame_id} - {self.frame_index} - {self.os_path} - {self.video_id}'
