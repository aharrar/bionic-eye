from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from Entities.base import Base


class Video(Base):
    __tablename__ = "video"
    video_id = Column(Integer, primary_key=True)
    location_name = Column(String, unique=False)
    os_path = Column(String, unique=False)
    frames_amount = Column(Integer, unique=False)
    frames = relationship('Frame', backref="Video", cascade="all, delete-orphan")

    def __init__(self, video_id: int, location_name: str, os_path: str, frames_amount: int):
        self.video_id = video_id
        self.location_name = location_name
        self.os_path = os_path
        self.frames_amount = frames_amount

    def __repr__(self):
        return f'{self.video_id} - {self.location_name} - {self.frames_amount} - {self.os_path} '
